# Delivery App Web

Food delivery app built with React

## Setup

* Yarn: `yarn`
* npm: `npm i`

## Running
  * Yarn: `yarn start`
  * npm: `npm run start`

## Powered by Alphanet Digital

Miguel Peres