import React from 'react';
import { Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import Private from './private';
import Guest from './guest';

import Login from '../pages/Login';
import Products from '../pages/Products';
import NewCategory from '../pages/NewCategory';

const Routes = () => (
    <ConnectedRouter history={history}>
      <Switch>
        <Guest exact path="/login" component={Login} />
        <Private path="/newcategory" component={NewCategory} />
      </Switch>
    </ConnectedRouter>
  );
  
  export default Routes;