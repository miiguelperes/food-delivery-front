import React from 'react';
import { Switch, withRouter, BrowserRouter } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import history from './history';

import Private from './private';
import Guest from './guest';

import Login from '../pages/Login';
import Orders from '../pages/Orders';
import Drawer from '../pages/Drawer';
import Products from '../pages/Products';
import NewCategory from '../pages/NewCategory';

const Routes = () => (
  <BrowserRouter forceRefresh={true}history={history}>
   
      <Guest exact path="/login" component={Login} />
      <Private path="/products" component={Products} />
      <Private path="/newcategory" component={NewCategory} />
      <Private exact path="/" component={Orders} />
 
  </BrowserRouter>
);

export default Routes;
