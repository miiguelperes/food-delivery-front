/* eslint-disable react/prop-types */
import React from 'react';
import { Route, Redirect, BrowserRouter } from 'react-router-dom';

import { createBrowserHistory } from "history";
import store from '../store';
const history = createBrowserHistory();
const PrivateRoute = ({ component: Component, ...rest }) => (
  <BrowserRouter forceRefresh={true}>
  <Route
    forceRefresh={true}
    history={history}
    {...rest}
    render={props => (store.getState().auth.signedIn ? (
      <Component {...props} />
    ) : (
      <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    ))
    }
  /></BrowserRouter>
);

export default PrivateRoute;
