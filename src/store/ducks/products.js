/**
 * Action Types
 */
export const Types = {
  GET_REQUEST: 'products/GET_REQUEST',
  GET_SUCCESS: 'products/GET_SUCCESS',
  GET_ERROR: 'products/GET_ERROR',
};

/**
 * Action Creators
 */

export const Creators = {
  getProductRequest: () => ({ type: Types.GET_REQUEST }),

  getProductsuccess: data => ({
    type: Types.GET_SUCCESS,
    payload: { data },
  }),

  getProductError: error => ({
    type: Types.GET_SUCCESS,
    payload: { error },
  }),
};

/**
 * Reducer
 */

const INITIAL_STATE = {
  data: [],
  loading: false,
  error: null,
};

export default function products(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true, error: null };
    case Types.GET_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        error: null,
      };
    case Types.GET_ERROR:
      return { ...state, loading: false, error: action.payload.error };
    default:
      return state;
  }
}
