import { call, put } from 'redux-saga/effects';
import api from '../../services/api';

import { Creators as productsActions } from '../ducks/products';
import { Creators as ErrorActions } from '../ducks/error';

export function* getProducts() {
  try {
    const response = yield call(api.get, '/menu');

    yield put(ErrorActions.hideError());
    yield put(productsActions.getProductsuccess(response.data));
  } catch (err) {
    yield put(ErrorActions.setError('User not found or wrong credentials'));
  }
}
