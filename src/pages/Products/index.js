import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as ProductsActions } from '../../store/ducks/products';
import { Creators as AuthActions } from '../../store/ducks/auth';
import normalizeData from '../../helpers/normalizeData';

import {
  Container,
  Header,
  Logo,
  UserInfo,
  LogoContainer,
  BrandName,
  UserName,
  LogoutButton,
  Content,
  PageTitle,
  OrdersContainer,
} from './styles';

import Product from '../../components/Product';

import LogoSVG from '../../assets/logo.svg';
import { FiPlusCircle } from 'react-icons/fi';

import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Avatar from '@material-ui/core/Avatar';
import api, { imagesURL } from '../../services/api';


const Products = ({
  user, products, getProductRequest, logout, loading,
}) => {
  useEffect(() => {
    const getProducts = async () => {
      await getProductRequest();

    };
    getProducts();
  }, [getProductRequest]);

  return (
    <Container>
      <Content>
        <PageTitle>Cardápio</PageTitle>
        <Link className="new-category" to="/newcategory" style={{
          color: '#41414d',
          alignSelf: 'flex-end',
          fontSize: '15px',
          textDecoration: 'none',
          marginBottom: '20px',
          fontWeight: '500',
        }}>
          <FiPlusCircle size={14} color="#E02041" />
          {" "}Adicionar nova categoria
        </Link>

        <OrdersContainer>
          {loading && <Loader type="Oval" color="#0b2031" height="100" width="100" />}
          {!!products.length
            && products.map((item, index) => (

              <ExpansionPanel key={index} style={{ minWidth: '100%', borderRadius: 8 }}>

                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Avatar alt="Remy Sharp" src={imagesURL + '/' + item.image} />
                  <div style={{ margin: 10 }}>{item.name}</div>
                </ExpansionPanelSummary>
                
                <Link className="new-category" to="newcategory" style={{
                      color: '#41414d',
                      fontSize: '15px',
                      textDecoration: 'none',
                      fontWeight: '500',
                    }}>
                      <FiPlusCircle size={14} color="#E02041" />
                      {" "}Novo produto
                  </Link>

                <ExpansionPanelDetails>                  
                  <div style={{ minWidth: '100%', flex: 1, flexWrap: 'wrap' }}>

                    <h3>Descrição</h3>
                    <p>{item.description}</p>
                    <h3>Tempo de preparo</h3>
                    <p>{item.preparation_time} minutos</p>
                    <h3>Tipos:</h3>
                    {!!item.product.length && item.product.map((type, index2) => (
                      <div style={{ border: '1px solid black', margin: 20, padding: 20, borderColor: "#000", flex: 1 }} key={index2}>
                        <div style={{ width: '100%', padding: 10, background: '#ccc' }}><b>{type.name}</b></div>
                        <p>Tamanhos:</p>
                        <div>
                          {!!type.Sizes.length && type.Sizes.map((size, index3) => (
                            <div key={index3}>
                              <p><b>{size.description}</b> - Preço:{size.Price.price}</p>
                            </div>
                          ))}
                        </div>
                      </div>
                    ))}
                  </div>
                </ExpansionPanelDetails>
              </ExpansionPanel>

            ))}

        </OrdersContainer>
      </Content>
    </Container >
  );
};

Product.propTypes = {
  item: PropTypes.shape({
    type: PropTypes.shape({
      image: PropTypes.string,
      name: PropTypes.string,
    }),
    size: PropTypes.shape({
      description: PropTypes.string,
    }),
  }).isRequired,
};

const mapStateToProps = state => ({
  user: state.auth.loggedUser,
  products: state.products.data,
  loading: state.products.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    ...ProductsActions,
    ...AuthActions,
  },
  dispatch,
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Products);
