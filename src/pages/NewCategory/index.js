import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import api from '../../services/api';
import { FiArrowLeft } from 'react-icons/fi';
import Order from '../Orders/index'
import store from '../../store/index';

import './styles.css';

export default function NewCategory() {
    const [name, setName] = useState('');
    const [renderO, setRenderO] = useState('');
    const [description, setDescription] = useState('');
    const [preparation_time, setPreparation_time] = useState('');

    const history = useHistory();
    const { token } = store.getState().auth;

    async function handleNewCategory(e) {
        e.preventDefault();

        const data = {
            name,
            description,
            preparation_time,
        };

        try {
            await api.post('products', data, {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            })

            history.push('/products');
        } catch (err) {
            alert('Erro ao cadastrar nova categoria, tente novamente.');
        }
    }

    const rederOrder = () =>{
        if(renderO){ 
            return <Order /* callback={callbackOrder}*//>; 
        }
    }
    
     /*  
      props.callback({nome, descri,})
    typos = []
    var produtoFinal = {
        nome, descr, image, typos, 
    }
    const callbackOrder = (retorno) => {
        retorno
        typos.push(retorno)
    };*/
    const changeRenderO = () => {
        setRenderO(!renderO);
        
    };
    return (
        <div className="new-category-container">
            <div className="content">
                <h1>Nova categoria</h1>
                <Link className="back-products" to="/products" style={{
                    color: '#41414d',
                    alignSelf: 'flex-end',
                    fontSize: '15px',
                    textDecoration: 'none',
                    marginBottom: '20px',
                    fontWeight: '500',
                }}>
                    <FiArrowLeft size={14} color="#E02041" />
                    {" "}Voltar para cardápio
            </Link>
                <form onSubmit={handleNewCategory}>
                    <input
                        placeholder="Nome da categoria"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />

                    <textarea
                        placeholder="Descrição"
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                    />

                    <input
                        placeholder="Tempo de preparo"
                        value={preparation_time}
                        onChange={e => setPreparation_time(e.target.value)}
                    />
                    <button onClick={changeRenderO} className="buttonCategory" type="submit">Cadastrar</button>
                    { rederOrder() }
                </form>
            </div>
        </div>
    )
}