import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Redirect } from "react-router-dom";
import { Creators as AuthActions } from '../../store/ducks/auth';
import { FiArchive, FiClipboard, FiLogOut, FiSettings, FiUser, FiCreditCard } from 'react-icons/fi';
import {
    Container,
    Header,
    Logo,
    UserInfo,
    LogoContainer,
    BrandName,
    UserName,
    LogoutButton,
    Content,
    PageTitle,
    OrdersContainer,
} from './styles';

import LogoSVG from '../../assets/logo.svg';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch
} from "react-router-dom";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

function logout_() {
    window.location.href = "/login";
}

function ResponsiveDrawer(props, context) {
    var user = JSON.parse(localStorage.getItem("@DeliveryApp:user"))

    const { container } = props;
     
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    var menus = [{
        title: 'Pedidos',
        icon: <FiArchive style={{ width: '24px', height: '24px' }} />,
        route: '/'
    }, {
        title: 'Cardápio',
        icon: <FiClipboard style={{ width: '24px', height: '24px' }} />,
        route: '/products'
    }, {
        title: 'Clientes',
        icon: <FiUser style={{ width: '24px', height: '24px' }} />,
        route: '/clients'
    }]
    const drawer = (
        <div>
            <div className={classes.toolbar} />
            <Divider />
            <List>
                {menus.map((item, index) => (
                    <a key={index} href={item.route} style={{
                        color: '#0060B6',
                        textDecoration: 'none'
                    }}>
                        <ListItem button >
                            <ListItemIcon >{item.icon}</ListItemIcon>
                            <ListItemText primary={item.title} />
                        </ListItem>
                    </a>
                ))}
            </List>
            <Divider />
            <List>
                {['Pagamento', 'Configurações'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <FiCreditCard style={{ width: '24px', height: '24px' }} /> : <FiSettings style={{ width: '24px', height: '24px' }} />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
                <ListItem onClick={() => {props.store.dispatch({
                                    type: 'user/LOGOUT',
                                  }); user = false;window.location.reload() }} button >
                    <ListItemIcon> <FiLogOut style={{ width: '24px', height: '24px' }} /></ListItemIcon>
                    <ListItemText primary={'Sair'} />
                </ListItem>
            </List>
        </div>
    );
    var state = { redirect: 0 };
    if (state.redirect) {
        return <Redirect to={state.redirect} />
    }
    if (user)
        return (
            <div className={classes.root}>
                <CssBaseline />
                <AppBar position="fixed" className={classes.appBar}>

                    <Header>
                        <LogoContainer>
                            <Logo src={LogoSVG} />
                            <BrandName>Pizzaria Don Juan</BrandName>
                            </LogoContainer>
                        <UserInfo>
                            <UserName>{user ? user.name : ''}</UserName>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                edge="start"
                                onClick={handleDrawerToggle}
                                className={classes.menuButton}
                            >
                                <MenuIcon />
                            </IconButton>

                        </UserInfo>
                    </Header>
                </AppBar>
                <nav className={classes.drawer} aria-label="mailbox folders">
                    {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                    <Hidden smUp implementation="css">
                        <Drawer
                            container={container}
                            variant="temporary"
                            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                            open={mobileOpen}
                            onClose={handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            ModalProps={{
                                keepMounted: true, // Better open performance on mobile.
                            }}
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                </nav>
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    {props.page}
                </main>
            </div>
        ); else return props.page
}
ResponsiveDrawer.propTypes = {

    container: PropTypes.any,
};

export default ResponsiveDrawer;