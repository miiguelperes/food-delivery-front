import React from 'react';
import { Provider } from 'react-redux';
import ReduxToastr from 'react-redux-toastr';

import './config/ReactotronConfig';

import GlobalStyle from './styles/global';

import { Wrapper } from './styles/components';
import Drawer from './pages/Drawer';
import Routes from './routes';
import store from './store/index';

export default function App() {
  var outDrawer = (<Wrapper  context={Routes} style={{marginTop:'24px'}}>
    <GlobalStyle />
    <Routes />
    <ReduxToastr />
  </Wrapper>);
  return (
    <Provider store={store}>
        <Drawer store={store} context={this} page={outDrawer}/>
    </Provider>
  );
}
